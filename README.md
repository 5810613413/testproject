**กลุ่ม Lost&Found**

* จีระวัฒน์ หนักแน่น 5710450031
* ธีระพล ไวยนิยี 5810613413
* ศิริรัตน์ บุณยัษเฐียร 5910490035
* ภูริณัฐ ธรรมราช 5910613347

---
##### Heroku:  https://lost-and-found-project.herokuapp.com
---
## Test:
>>เตรียมข้อมูล:
```sh
        $ cd testproject
        $ bundle install --without production
        $ rails db:migrate RAILS_ENV=test
```
>>ทดสอบ Story (Request Test):
```sh
        $ rspec spec/requests/signups_spec.rb
        $ rspec spec/requests/logins_spec.rb
        $ rspec spec/requests/informs_spec.rb
        $ rspec spec/views/informs/show.html.erb_spec.rb
```
        
>>ทดสอบ Model:
```sh
        $ rspec spec/model/user_spec.rb
        $ rspec spec/model/inform_spec.rb
```
>>ทดสอบ Controller:
```sh
        $ rspec spec/controllers/static_pages_controller_spec.rb
        $ rspec spec/controllers/users_controller_spec.rb
        $ rspec spec/controllers/sessions_controller.rb
        $ rspec spec/controllers/informs_controller_spec
```