class CreateInforms < ActiveRecord::Migration[5.1]
  def change
    create_table :informs do |t|
      t.integer :user_id
      t.string :item_name
      t.string :detail
      t.string :date
      t.string :location
      t.string :contact
      t.string :pic_url
      t.string :inform_type

      t.timestamps
    end
  end
end
