class CreateFoundItems < ActiveRecord::Migration[5.1]
  def change
    create_table :found_items do |t|
      t.string :item_name
      t.text :detail
      t.datetime :date
      t.string :location
      t.string :contact
      t.string :pic_url
      t.timestamps
    end
  end
end
