json.extract! inform, :id, :user_id, :item_name, :detail, :date, :location, :contact, :pic_url, :type, :created_at, :updated_at
json.url inform_url(inform, format: :json)
