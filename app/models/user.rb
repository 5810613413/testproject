class User < ApplicationRecord
has_many :informs
validates :username,  presence: true, length: { maximum: 40 } , uniqueness: { case_sensitive: false }
has_secure_password
validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
validates :contact,  presence: true, length: { maximum: 255 }
validates :tel_number,  presence: true, length: { is: 10 } , numericality: { only_integer: true }
    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end
end