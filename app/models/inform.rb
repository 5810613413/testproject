class Inform < ApplicationRecord
belongs_to :user, optional: true
validates :user_id,  presence: true
validates :item_name,  presence: true, length: { maximum: 40 }
validates :detail,  presence: true, length: { maximum: 300 }
validates :date,  presence: true, length: { minimum: 8 }
validates :location,  presence: true, length: { maximum: 250 }
validates :contact,  presence: true, length: { maximum: 100 }
validates :pic_url,  presence: true, length: { maximum: 100 },url: true


end
