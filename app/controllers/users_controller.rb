class UsersController < ApplicationController
before_action :check_for_cancel, :only => [:create]

    def index
    end
    # define attr
    def new
        @user = User.new
    end
    
    def user_inform
        @informs = Inform.where(:user_id => current_user.id)
    end
    
    # create user
    def create
        @user = User.new(user_params)
        if @user.save
            flash[:info] = "Create Success!!."
            redirect_to root_url
        else
            render 'new'
        end
    end
    
    def show
        @user = User.find(params[:id])
    end
    
    def check_for_cancel
        if params[:commit] == "Reset"
        redirect_to '/signup'
        end
    end
    
    private
    #check input
    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation, :contact, :tel_number)
    end
    
    

end