Rails.application.routes.draw do
  resources :informs
  resources :users do
    collection do
      get :user_inform
    end
  end
  
  get    '/about',  to: 'static_pages#about'
  get    '/edit',   to: 'users#user_inform'
  get    '/signup',  to: 'users#new'
  get    '/losts',    to: 'informs#lost'
  get    '/founds',    to: 'informs#found'   
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  
  root 'informs#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
