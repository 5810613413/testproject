require 'rails_helper'

describe StaticPagesController  do 
    
    describe 'about' do
        it 'check view about in controller' do
            get :about
            expect(response).to render_template('about')
        end
    end
#     describe 'found' do
#         it 'check view found in controller' do
#             get :founds
#             expect(response).to render_template('new')
#         end
#     end
#   describe 'lost' do
#         it 'check view lost in controller' do
#             get :losts
#             expect(response).to render_template('losts')
#         end
#     end
end