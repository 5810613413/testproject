require 'rails_helper'

RSpec.describe Inform, type: :model do
    fixtures :inform
    before :each do
        @inform_test = inform(:car)
    end
  it "is valid with valid attributes" do
        expect(@inform_test).to be_valid
    end
    
    it "is not valid without a user_id" do 
        @inform_test.user_id = nil
        expect(@inform_test).to_not be_valid
    end
    
    it "is not valid without a item_name" do 
        @inform_test.item_name = nil
        expect(@inform_test).to_not be_valid
    end
    
    it "is not valid without a detail" do 
        @inform_test.detail = nil
        expect(@inform_test).to_not be_valid
    end
    
    it "is not valid without a date"do 
        @inform_test.date = nil
        expect(@inform_test).to_not be_valid
    end
    
    it "is not valid without a location"do 
        @inform_test.location = nil
        expect(@inform_test).to_not be_valid
    end
    it  "date should have a minimum length" do
        @inform_test.date = @inform_test.date = ("i" * 5)
        expect(@inform_test).to_not be_valid
        ### update ####
         @inform_test.date = @inform_test.date = ("i" * 8)
        expect(@inform_test).to be_valid
    end
end