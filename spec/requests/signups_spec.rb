require 'rails_helper'

### Test for creating an account

require 'rails_helper'
    
describe "Spec for Sign Up",type: :request  do     
    it "should create new user account:valid" do     
        #redirect to creatr of userController
    	post '/users' ,params:{user:{username: "annan",    
    	                    password: "12345678" ,
    	                    password_confirmation:"12345678",
    	                    contact:"Marcus Street,Birmingham",
                        	tel_number:"0809921754"     
                        	}}
      expect(response).to redirect_to(root_path) 
      #expect(response).to render_template(root_path) 
    end
    it "should create new user invalid without password" do     
            #redirect to creatr of userController
    	post '/users' ,params:{user:{username: "annan",    
    	                             password: "" ,
        	                         password_confirmation:"12345678",
        	                         contact:"Marcus Street,Birmingham",
                            	     tel_number:"0809921754"     
                            	      }}
      expect(response).to render_template('new') 
    end
    it "should create new user invalid without password_confirmation" do     
    #redirect to creatr of userController
    	post '/users' ,params:{user:{username: "annan",    
        	                    password: "12345678" ,
        	                    password_confirmation:"",
        	                    contact:"Marcus Street,Birmingham",
                            	tel_number:"0809921754"     
                            	}}
      expect(response).to render_template('new') 
    end
end
 