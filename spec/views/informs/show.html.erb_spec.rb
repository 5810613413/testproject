require 'rails_helper'

RSpec.describe "informs/show", type: :view do
  before(:each) do
    @inform = assign(:inform, Inform.create!(
      :user_id => 1,
      :item_name => "Item Name",
      :detail => "Detail",
      :date => "25092558",
      :location => "Location",
      :contact => "Contact",
      :pic_url => "https://www.google.co.th/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
      :inform_type => "lost"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Item Name/)
    expect(rendered).to match(/Detail/)
    expect(rendered).to match(/25092558/)
    expect(rendered).to match(/Location/)
    expect(rendered).to match(/Contact/)
    expect(rendered).to match(/lost/)
  end
end
